Source: ocaml-speex
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Kyle Robbertze <paddatrapper@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-buildinfo,
 ocaml,
 dh-ocaml (>= 1.2),
 libspeex-dev,
 libogg-ocaml-dev (>= 0.7.0),
 ocaml-dune,
 libdune-ocaml-dev,
 pkg-config
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: http://savonet.sourceforge.net/
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml-speex.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml-speex

Package: libspeex-ocaml
Architecture: any
Depends: ${ocaml:Depends}, ${shlibs:Depends}, ${misc:Depends}
Provides: ${ocaml:Provides}
Description: OCaml interface to the speex library
 This package provides an interface to the speex library for
 OCaml programmers.
 .
 Speex is an audio codec especially designed for compressing voice at low
 bit-rates for applications such as voice over IP (VoIP).
 .
 This package contains only the shared runtime stub libraries.

Package: libspeex-ocaml-dev
Architecture: any
Depends: ${ocaml:Depends},
         ${shlibs:Depends},
         libspeex-dev,
         libogg-ocaml-dev,
         libspeex-ocaml (= ${binary:Version}),
         ocaml-findlib, ${misc:Depends}
Provides: ${ocaml:Provides}
Description: OCaml interface to the speex library
 This package provides an interface to the speex library for
 OCaml programmers.
 .
 Speex is an audio codec especially designed for compressing voice at low
 bit-rates for applications such as voice over IP (VoIP).
 .
 This package contains all the development stuff you need to develop
 OCaml programs which use ocaml-speex.
